#include "ReceiverModel.h"
#include "machine-monitor.h"

#include <boost/bind.hpp>
#include <iostream>

using namespace std;
namespace ba = boost::asio;
using ba::ip::udp;


constexpr size_t RECEIVE_BUFFER_SIZE = 512; // probably large enough for most messages!


UdpReceiver::UdpReceiver(
	boost::asio::io_context& io_context, 
	int port, 
	UpdateMessage *output,
	CallBack_t handle_callback)
	: socket_(io_context, udp::endpoint(udp::v4(), port))
	, recv_buffer_(RECEIVE_BUFFER_SIZE)
	, output_{ output }
	, handle_callback_ { handle_callback }
{
	start_receive();
}

void UdpReceiver::start_receive()
{
	socket_.async_receive_from(
		boost::asio::buffer(recv_buffer_), remote_endpoint_,
		boost::bind(&UdpReceiver::handle_receive, this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
}

void UdpReceiver::handle_receive(const boost::system::error_code& error,
	std::size_t bytes_transferred)
{
	if (!error || error == boost::asio::error::message_size)
	{
		try {
			{
				lock_guard<mutex> guard(output_->m);
				output_->message = move(string(recv_buffer_.data(), bytes_transferred));
			}

			handle_callback_(output_);
		}
		catch (std::exception &e)
		{
			cerr << "**Exception handling received message: " << e.what() << endl;
		}

		start_receive();
	}
}
