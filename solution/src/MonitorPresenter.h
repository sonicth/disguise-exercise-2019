#pragma once

struct UpdateMessage;

class MessagePresenter
{
	struct PrivateData;
	PrivateData *priv;
public:
	MessagePresenter();
	~MessagePresenter();

	// pass messages to update states and present
	void updateMessageSession(UpdateMessage *session_message);
	void updateMessageMachine(UpdateMessage *machine_message);

private:
	// present, following an update; called by updateMessage*()
	void present() const;

};
