#include "machine-monitor.h"
#include "MonitorPresenter.h"
#include "ReceiverModel.h"

#include <vector>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/bind.hpp>


using namespace std;


int main()
{
	namespace ba = boost::asio;

	constexpr int SESSION_PORT = 7106;
	constexpr int PULSE_PORT = 7104;

	try
	{
		ba::io_context io_context;
		UpdateMessage session_message, machine_message;		
		MessagePresenter presenter;

		UdpReceiver receiver1(io_context, SESSION_PORT, &session_message, 
			boost::bind(&MessagePresenter::updateMessageSession, &presenter, _1));
		UdpReceiver receiver2(io_context, PULSE_PORT, &machine_message, 		
			boost::bind(&MessagePresenter::updateMessageMachine, &presenter, _1));
		io_context.run();
	
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}


