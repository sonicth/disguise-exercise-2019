#pragma once

namespace boost { namespace asio { class io_context;  } }
struct UpdateMessage;


#include <boost/asio.hpp>
#include <boost/function.hpp>
#include <vector>

class UdpReceiver
{
public:
	using CallBack_t = boost::function<void(UpdateMessage *)>;

	UdpReceiver(
		boost::asio::io_context& io_context, 
		int port, 
		UpdateMessage *output,
		CallBack_t handle_callback);

private:
	void start_receive();
	void handle_receive(const boost::system::error_code& error, std::size_t bytes_transferred);

	boost::asio::ip::udp::socket socket_;
	boost::asio::ip::udp::endpoint remote_endpoint_;
	std::vector<char> recv_buffer_;
	UpdateMessage *output_;
	CallBack_t handle_callback_;
};
