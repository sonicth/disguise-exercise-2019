#include "MonitorPresenter.h"
#include "machine-monitor.h"

#include <vector>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <iterator>

#include <boost/algorithm/string/split.hpp>
#include <boost/lexical_cast.hpp>

#if defined(WIN32)
#include "Windows.h"
#endif

using namespace std;


const string SEPARATOR = "\t";
const string SEPARATOR_LIST = ", ";


struct MachineState
{
	int fps;
	string version;
};

using Strings_t = std::vector<std::string>;
using SessionName = std::string;
using MachineName = std::string;

struct SessionStates
{
	using MachineList_t = std::unordered_set<MachineName>;
	std::unordered_map<SessionName, MachineName> session_master;
	std::unordered_map<SessionName, MachineList_t> session_slave_machines;

	void startSession(
		SessionName const &session_name,
		MachineName &&master,
		MachineList_t &&slaves)
	{
		session_master[session_name] = move(master);
		session_slave_machines[session_name] = move(slaves);
	}
};

using MachineStates_t = std::unordered_map<MachineName, MachineState>;

class TerminalOut
{

#if defined(WIN32)
	DWORD dwOriginalOutMode = 0;
	HANDLE hOut;
#endif
public:
	TerminalOut()
	{
#if defined(WIN32)
		// Set output mode to handle virtual terminal sequences
		hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		if (hOut == INVALID_HANDLE_VALUE)
		{
			throw false;
		}

		if (!GetConsoleMode(hOut, &dwOriginalOutMode))
		{
			throw  false;
		}

		DWORD dwRequestedOutModes = ENABLE_VIRTUAL_TERMINAL_PROCESSING | DISABLE_NEWLINE_AUTO_RETURN;

		DWORD dwOutMode = dwOriginalOutMode | dwRequestedOutModes;
		if (!SetConsoleMode(hOut, dwOutMode))
		{
			// we failed to set both modes, try to step down mode gracefully.
			dwRequestedOutModes = ENABLE_VIRTUAL_TERMINAL_PROCESSING;
			dwOutMode = dwOriginalOutMode | dwRequestedOutModes;
			if (!SetConsoleMode(hOut, dwOutMode))
			{
				// Failed to set any VT mode, can't do anything here.
				throw - 1;
			}
		}
#endif
	}

	~TerminalOut()
	{
#if defined(WIN32)
		// restore output
		SetConsoleMode(hOut, dwOriginalOutMode);
#endif
	}

	template <typename Toutput>
	void printScreen(Toutput const &screen_str)
	{
		cerr
			<< "\x1B[2J\x1B[H" // code to clear terminal screen
			<< screen_str
			;
	}
};

std::string displaySessions(SessionStates const &session_states)
{
	stringstream ss;

	if (session_states.session_master.empty())
		return {};

	for (auto &ses : session_states.session_master)
	{
		SessionName session_name = ses.first;
		auto master = ses.second;

		ss << session_name << ": " << SEPARATOR << "[" << master << "]";

		SessionStates::MachineList_t const &slaves = session_states.session_slave_machines.find(session_name)->second;
		for (auto &machine : slaves)
		{
			ss << SEPARATOR_LIST;
			ss << machine;				
		}
		ss << endl;
	}

	return ss.str();
}

std::string displayMachineStates(MachineStates_t const &machine_states)
{
	stringstream ss;

	if (machine_states.empty())
		return {};

	// print header
	ss << "*name*"
		<< SEPARATOR << "*fps*"
		<< SEPARATOR << "*version*"
		<< endl;

	for (auto &ms : machine_states)
	{
		auto &name = ms.first;
		auto &state = ms.second;

		ss << name
			<< SEPARATOR << state.fps
			<< SEPARATOR << state.version
			<< endl;
		;
	}

	return ss.str();
}

Strings_t splitMessage(string const &message)
{
	using namespace boost::algorithm;
	Strings_t strings;
	// split by '|'
	split(strings, message, [](auto s) { return s == '|'; }, token_compress_on);
	return strings;
}

void parseUpdateSessions(string const &message, SessionStates &session_states)
{
	
	if (message.empty())
		return;

	auto strings = splitMessage(message);

	if (strings.empty())
		return;

	
	if (strings[0] == "MACHINE")
	{
		//machine_names.emplace_back(strings[1]);
		//session_name = strings[2];
	}
	else if (strings[0] == "SESSION2")
	{
		if (strings.size() < 4)
			throw std::runtime_error("Parsing sessions: session definition needs to have at least 4 tokens");

		// slave machines start at 3
		auto slaves_start = next(begin(strings), 3);

		session_states.startSession(
			strings[1], 
			move(strings[2]),
			SessionStates::MachineList_t(slaves_start, end(strings)));
	}
	else
	{
		throw std::runtime_error("Parsing sessions: invalid message format");
	}

}

void parseUpdateMachines(string const &message, MachineStates_t &machine_states)
{
	using namespace boost::algorithm;

	if (message.empty())
		return;

	auto strings = splitMessage(message);

	if (strings.size() != 4)
	{
		//ignore messages of other sizes
		return;
	}

	MachineName machine_name;
	if (strings[0] == "MACHINESTATUS")
	{
		machine_name = strings[1];

		// insert/overwrite machine state
		MachineState &state = machine_states[machine_name];
		state.version = strings[2];
		state.fps = boost::lexical_cast<decltype(state.fps)>(strings[3]);
	}
	else
	{
		throw std::runtime_error("Parsing machine status: invalid message format");
	}
}


struct MessagePresenter::PrivateData
{
	TerminalOut out;
	SessionStates session_states;
	MachineStates_t machine_states;
};

MessagePresenter::MessagePresenter()
	: priv{ new PrivateData }
{

}
void MessagePresenter::updateMessageSession(UpdateMessage *session_message)
{
	{
		lock_guard<mutex> guard1(session_message->m);
		parseUpdateSessions(session_message->message, priv->session_states);
	}
	present();
}
void MessagePresenter::updateMessageMachine(UpdateMessage *machine_message)
{
	{
		lock_guard<mutex> guard2(machine_message->m);
		parseUpdateMachines(machine_message->message, priv->machine_states);
	}
	present();
}
void MessagePresenter::present() const
{
	auto formatted_output = 
		displaySessions(priv->session_states)
		+ "\n"
		+ displayMachineStates(priv->machine_states);
	priv->out.printScreen(formatted_output);
}

MessagePresenter::~MessagePresenter()
{
	delete priv;
}
