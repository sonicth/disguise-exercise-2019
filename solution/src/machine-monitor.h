#pragma once
#include <string>
#include <mutex>

struct UpdateMessage
{
	std::string message;
	std::mutex m;
};
